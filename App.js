import * as React from "react";
import { Text, View, StyleSheet, TextInput, Button } from "react-native";
import * as SecureStore from "expo-secure-store";

async function save(key, value) {
  await SecureStore.setItemAsync(key, value);
}

async function getValueFor(key) {
  let result = await SecureStore.getItemAsync(key);
  if (result) {
    alert("🔐 Here's your value 🔐 \n" + result);
  } else {
    alert("No values stored under that key.");
  }
}

export default function App() {
  const [key, onChangeKey] = React.useState("Nhập tài khoản vào linh");
  const [value, onChangeValue] = React.useState("Nhap pass vào linh");

  return (
    <View style={styles.container}>
      <Text style={styles.paragraph}>Get data luôn nha!</Text>
      {}

      <TextInput
        style={styles.textInput}
        clearTextOnFocus
        onChangeText={(text) => onChangeKey(text)}
        value={key}
      />
      <TextInput
        style={styles.textInput}
        clearTextOnFocus
        onChangeText={(text) => onChangeValue(text)}
        value={value}
      />
      {}
      <Button
        title="Save"
        onPress={() => {
          save(key, value);
          onChangeKey("Nhập tài khoản vào linh");
          onChangeValue("Nhap pass vào linh");
        }}
      />

      <Text style={styles.paragraph}>Nhập lại tài khoản tìm mật khẩu</Text>
      <TextInput
        style={styles.textInput}
        onSubmitEditing={(event) => {
          getValueFor(event.nativeEvent.text);
        }}
        placeholder="Enter the key for the value you want to get"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingTop: 10,
    backgroundColor: "#ecf0f1",
    padding: 8,
  },
  paragraph: {
    marginTop: 34,
    margin: 24,
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
  },
  textInput: {
    height: 35,
    borderColor: "gray",
    borderWidth: 0.5,
    padding: 4,
  },
});
